package gov.mil.esp.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;


@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties(UriConfiguration.class)
@RestController
public class EspGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(EspGatewayApplication.class, args);
	}
	
	// tag::route-locator[]
	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder, UriConfiguration uriConfiguration) {
		
		return builder.routes()
			.route(p -> p
				.path("/esp-aspirante/**")
				.filters(f -> f.rewritePath("/esp-aspirante", "/esp-aspirante/"))
				.uri("lb://esp-aspirante"))
			.route(p -> p
					.path("/formato/**")
					//.filters(f -> f.rewritePath("/formato", "/formato/"))
					.uri("lb://esp-pdf"))
			.route(p -> p
					.path("/esp-proceso/**")
					//.filters(f -> f.rewritePath("/formato", "/formato/"))
					.uri("lb://esp-proceso"))
			.route(p -> p
					.path("/esp-usuario/**")
					//.filters(f -> f.rewritePath("/formato", "/formato/"))
					.uri("lb://esp-usuario"))
			.route(p -> p
					.path("/esp-authorization/**")
					//.filters(f -> f.rewritePath("/formato", "/formato/"))
					.uri("lb://esp-authorization"))
			/*.route(p -> p
				.host("*.hystrix.com")
				.filters(f -> f
					.hystrix(config -> config
						.setName("mycmd")
						.setFallbackUri("forward:/fallback")))
				.uri(""))*/
			.build();
	}
	// end::route-locator[]

	// tag::fallback[]
	@RequestMapping("/fallback")
	public Mono<String> fallback() {
		return Mono.just("fallback");
	}
	// end::fallback[]

	
}


@ConfigurationProperties
class UriConfiguration {
	
	private String httpbin = "http://httpbin.org:80";

	public String getHttpbin() {
		return httpbin;
	}

	public void setHttpbin(String httpbin) {
		this.httpbin = httpbin;
	}
}


